# Project 1: Algorithms

A company running a forum platform has asked us to create a new application based on the one they had already. Since there's data from previous forum threads, the company wants to import it into the new app. Sadly, we only have the backups from the old threads, and the information looks like this:

```
[thread: 35243]
### 4723 () ["laugh"]
- user_1:
This looks kinda complex at first sight.

### 7273 (4723) ["wow", "wow", "angry", "laugh"]
- user_2:
That's because you're weak LMAO

### 26391 (7273) ["wow"]
- user_1:
Try to do it yourself then >:/

### 28461 (26391) []
- user_3:
Watch out guys, we've got a badass over here

### 8264 (4723) ["love", "love"]
- user_4:
You can do it!

### 4946 (8264) []
- user_1:
Thanks :)

### 4051 (8264) ["angry"]
- user_2:
He's going to give up pretty soon, wait and see.
```

## Instructions

You have to parse the information from the old threads and display it in an organized way running on a prototype. The meaning of each part ot a thread is the following:

- Thread ID.
- Comment, initialized by `###`.
- Comment ID.
- ID of a comment this is replying to inside of parenthesis `()`.
- A list of reactions from other users to that comment.
- Username of the user who wrote the comment, denoted by `-`.
- Text of the comment.

```
[thread: thread_id]

### comment_id (replaying_to_id) ["reaction1", "reaction2"]
- username:
The text of the comment.
```


### 1. Display the nested comments

Given that there are comments in reply to others, they must be nested like this:

```
- comment_1: parent
    - comment_2: reply to comment_1
        - comment_3 reply to comment_2
            - comment_4: reply to comment_3
    - comment_5: reply to comment_1
        - comment_6: reply to comment_5
        - comment_7: reply to comment_5
```

You have to parse the text file and display it in a nested way using whatever front-end you want.

### 2. Styling the comments

In order for the thread to be more readable, each comment has to be displayed like this:

```
Username:
This looks kinda complex at first sight.
(Reactions, instructions below)
```

For a thread, each username has to be displayed in the same **random** color. For example, `username_1` should be red in every comment and `username_2` should be green. How ever, running the algorithm again should create new colors for everyone.

### 3. Displaying reactions

The instructions must be grouped showing the count for all of them. For example:

```
["wow", "wow", "angry", "laugh"]
```

translates to:

```
wow: 2, angry: 1, laugh: 1
```

They must be displayed in every comment, sorted from the highest count to the lowest one.


### 4. Analytics 1: activeness

We will perform some data analysis in order to optimize the use of the platform. So every thread must contain an "Analytics" section on the bottom.

First, the company wants to add a feature for knowing how active each user is in a thread, so we will call this value `activeness`. For calculating this, it takes into account:

- The number of comments made by a user.
- The amount of words written by a user in the whole thread.

Both aspects are plotted in a chart like follows:

![activeness_chart](activeness.png)

Being the most active user the point that's further away from the origin (the zero value). You don't have to display the chart (although it would be great), you only need to display the most and least active users in the thread.

### 5. Analytics 2: thread popularity

Eventually, we will have to create a "trending" section for the forum platform, so we need to know how popular a thread is. Thread popularity is computed as follows:

```
(amout_of_users / amount_of_coments) * (amount_of_reactions + 1)
```

You have to display the thread popularity in the analytics section of the thread.


## Goal!

Something like this (thre prettier, the better):

![output](output.png)

## Restrictions

- You can use any technology you want.
- You can extract the data from a .txt file, a text field in a form or what ever source you want.
- The code for the project must be stored in a git repo, and the development process should follow the merge-request workflow.
